df <- read_csv(
  here("data", "clean", "10_chla_bloom_date_by_area_year_hexid.csv")
)

df <- df |>
  summarise(
    mean_chla_sd = sd(mean_chla, na.rm = TRUE),
    mean_chla_cv = sd(mean_chla, na.rm = TRUE) / mean(mean_chla, na.rm = TRUE),
    .by = hex_id
  )

write_csv(df, here("data", "clean", "11_chla_variation_coefficient.csv"))

st_read(here("data", "clean", "hex.gpkg")) |>
  inner_join(df) |>
  ggplot() +
  geom_sf(aes(fill = mean_chla_cv)) +
  scale_fill_viridis_c()

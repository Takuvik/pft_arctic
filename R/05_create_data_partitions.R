# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
# AUTHOR:       Philippe Massicotte
#
# DESCRIPTION:  Create a data partition with the calculated PFT. This will make
# further processing much quicker.
# <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

rm(list = ls())

open_dataset(here("data", "clean", "pft", "parquet")) |>
  write_dataset(
    here("data", "clean", "pft", "hive"),
    partitioning = c("year", "area")
  )

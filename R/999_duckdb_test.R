con <- dbConnect(duckdb())

dbSendQuery(
  con,
  "
   CREATE
   OR REPLACE VIEW v AS
   SELECT
     *
   FROM
     'data/clean/pft/hive/**/*.parquet';
  "
)

dbSendQuery(con, "DESCRIBE v;") |>
  dbFetch()

dbSendQuery(
  con,
  "COPY v TO '~/Desktop/ttttt/' (FORMAT PARQUET, COMPRESSION 'zstd', PARTITION_BY (pixel_class))"
)

tbl(
  con,
  "read_parquet('~/Desktop/ttttt/**/*.parquet', hive_partitioning = true)"
)

tbl(con, "v") |>
  count(year, area, pixel_class) |>
  mutate(percent = n / sum(n, na.rm = TRUE), .by = c(year, area)) |>
  arrange(year, area, pixel_class) |>
  collect()

tbl(con, "v") |>
  count(date) |>
  mutate(month = month(date, label = TRUE)) |>
  arrange(desc(n)) |>
  collect()

# This is fast! Directly use duckdb functions to read the data
tbl(con, "read_parquet('data/clean/pft/hive/**/*.parquet')") |>
  count(year, area, pixel_class) |>
  mutate(percent = n / sum(n, na.rm = TRUE), .by = c(year, area)) |>
  arrange(year, area, pixel_class) |>
  collect()

tbl(con, "read_parquet('data/clean/pft/hive/**/*.parquet')") |>
  count(year, area, pixel_class) |>
  mutate(percent = n / sum(n, na.rm = TRUE), .by = c(year, area)) |>
  arrange(year, area, pixel_class) |>
  collect()

df <- tbl(con, "read_parquet('data/clean/pft/hive/**/*.parquet')")

df |>
  glimpse()

df |>
  count(pixel_class, year = year(date)) |>
  collect()

df |>
  to_arrow() |>
  write_dataset("~/Desktop/test/", partitioning = "pixel_class")


dbDisconnect(con)

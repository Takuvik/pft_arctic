-- duckdb 
INSTALL spatial;

LOAD spatial;

CREATE
OR REPLACE TABLE HEX AS
SELECT
  *,
FROM
  st_read ('data/clean/hex.gpkg');

CREATE OR REPLACE VIEW df AS
SELECT
  *,
  st_point (latitude, longitude) AS geom,
  st_transform (geom, 'EPSG:4326', 'EPSG:3413') AS geom2
FROM
  read_parquet ('data/clean/pft/hive/**/*.parquet');

DESCRIBE df;

DESCRIBE HEX;

DROP INDEX IF EXISTS id_hex;

CREATE UNIQUE INDEX id_hex ON HEX(geom);

DROP INDEX IF EXISTS id_df;

CREATE INDEX id_df ON df (geom2);

SELECT
  *
FROM
  HEX
LIMIT
  10;

SELECT
  COUNT(*)
FROM
  HEX;

SELECT
  COUNT(*)
FROM
  df;

-- How to spatial join both tables. Hex is polygon and df point
CREATE OR REPLACE VIEW df_hex AS
SELECT
  *
FROM
  df,
  HEX
WHERE
  ST_Intersects (df.geom2, HEX.geom);

DESCRIBE df_hex;

SELECT
  COUNT(*)
FROM
  df_hex;

SELECT
  *
FROM
  df_hex
LIMIT
  10;

-- How to count the number of points per hexagon
-- COPY (SELECT * EXCLUDE ('geom', geom2, date) FROM df_hex) TO '~/Desktop/test.gpkg' WITH (FORMAT GDAL, DRIVER 'GPKG');
CREATE OR REPLACE VIEW test AS
SELECT
  id,
  pixel_class,
  DATE,
  n
FROM
  (
    SELECT
      q01.*,
      MAX(n) OVER (
        PARTITION BY
          id
      ) AS col01
    FROM
      (
        SELECT
          id,
          pixel_class,
          COUNT(*) AS n
        FROM
          df_hex
        GROUP BY
          id,
          pixel_class
      ) q01
  ) q01
WHERE
  (n = col01);

DESCRIBE test;

SELECT
  *
FROM
  test
LIMIT
  10;

COPY test TO '~/Desktop/test.csv'
WITH
  (DELIMITER ',', HEADER TRUE);

# Phytoplankton PFT in the Arctic

This is a project with Axel Vernay who's a French master student. This is a project where the student wants to learn the basics of remote sensing.

- <https://gitlab.com/Takuvik/pft_arctic>

## How to batch download Ocean Color data files

### Authentication

To download Ocean Color data, we must first create authentication files as follow: [**https://disc.gsfc.nasa.gov/data-access**](https://disc.gsfc.nasa.gov/data-access)**.**

Detailed information can be found here: <https://disc.gsfc.nasa.gov/information/howto?title=How%20to%20Access%20GES%20DISC%20Data%20Using%20wget%20and%20curl>

### Create a list of files

Then, we can create a text file (ex.: `url.txt`) that contains a list of all files to be downloaded. For one specific day, we need the 10 Rrs files:

```{bash}
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_412.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_443.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_469.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_488.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_531.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_547.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_555.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_645.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_667.4km.nc
https://oceandata.sci.gsfc.nasa.gov/cgi/getfile/AQUA_MODIS.20220509.L3m.DAY.RRS.Rrs_678.4km.nc
```

These are the files (Rrs) needed for 2022-05-09: <https://oceandata.sci.gsfc.nasa.gov/directdataaccess/Level-3%20Mapped/Aqua-MODIS/2022/129>

Note that there are no rrs data between 2022-04-01 and 2022-04-12.)

### Download the files

Then, use `wget` to download the data files included in `url.txt`.

```{bash}
wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --auth-no-challenge=on --keep-session-cookies --content-disposition -i url.txt
```

## Polygons

Polygons were drawn from [geojsonio](https://geojson.io/#new&map=2/0/20)
